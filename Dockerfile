FROM python:3.9.20-alpine3.20


RUN apk add --update make \
    && apk add bash bash-doc bash-completion \
    && apk add docker-cli docker-cli-compose \
    && apk add ansible \
    && pip3 install awscli ansible boto boto3 flake8 \
    && apk add --update curl \
    && curl -o /usr/local/bin/ecs-cli https://s3.amazonaws.com/amazon-ecs-cli/ecs-cli-linux-amd64-latest \
    && chmod +x /usr/local/bin/ecs-cli
